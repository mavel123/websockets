package entities

import (
	"context"
	"fmt"
	"log"
	"nhooyr.io/websocket"
	"strconv"
	"strings"
	"sync"
	"time"
)

const N = 2

func getCounter() func() int {
	count := 0
	return func() int {
		count++
		return count
	}
}

type clients struct {
	mu      sync.Mutex
	data    []map[int]*websocket.Conn
	counter func() int
}

func (c *clients) Add(ws *websocket.Conn) {
	id := c.counter()

	c.mu.Lock()
	defer c.mu.Unlock()

	hubCount := len(c.data)
	var lastHub map[int]*websocket.Conn
	if hubCount == 0 {
		lastHub = make(map[int]*websocket.Conn)
		c.data = append(c.data, lastHub)
	} else {
		lastHub = c.data[hubCount-1]
	}

	if len(lastHub)+1 <= N {
		lastHub[id] = ws
	} else {
		hub := map[int]*websocket.Conn{id: ws}
		c.data = append(c.data, hub)
	}
}

func (c *clients) Show() {
	c.mu.Lock()
	defer c.mu.Unlock()

	fmt.Printf("%+v\n", c.data)
}

func (c *clients) SendMessageToHub(hubNumber int) {
	indexOfSlice := hubNumber - 1

	c.mu.Lock()
	defer c.mu.Unlock()

	if len(c.data) <= indexOfSlice {
		log.Println("There is not this hub yet")
		return
	}

	hub := c.data[indexOfSlice]

	for id, conn := range hub {
		err := conn.Write(context.Background(), 2, []byte(fmt.Sprintf("It's %s", time.Now().Format(time.RFC822))))
		if err != nil {
			log.Printf("Could not send message to client: %d\n", id)
		}
	}
}

func (c *clients) SendMessageToClient(clientId int) {
	c.mu.Lock()
	defer c.mu.Unlock()

	for _, hub := range c.data {
		for id, conn := range hub {
			if id == clientId {
				err := conn.Write(context.Background(), 2, []byte(fmt.Sprintf("It's %s", time.Now().Format(time.RFC822))))
				if err != nil {
					log.Printf("Could not send message to client: %d\n", id)
				}
				return
			}
		}
	}

	log.Println("There is not this client: ", clientId)
}

func (c *clients) GetAllClientIds() string {
	c.mu.Lock()
	defer c.mu.Unlock()

	clientIds := make([]string, 1, 1)
	for _, hub := range c.data {
		for id := range hub {
			clientIds = append(clientIds, strconv.Itoa(id))
		}
	}

	return strings.Join(clientIds, " ")
}

func (c *clients) GetHubs() string {
	c.mu.Lock()
	defer c.mu.Unlock()

	hubIndexes := make([]string, 1, 1)
	for index := range c.data {
		hubIndexes = append(hubIndexes, strconv.Itoa(index))
	}

	return strings.Join(hubIndexes, " ")
}

func NewClients() *clients {
	return &clients{
		data:    []map[int]*websocket.Conn{},
		counter: getCounter(),
	}
}
