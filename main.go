package main

import (
	"chat/src/entities"
	"fmt"
	"github.com/c-bata/go-prompt"
	"io"
	"log"
	"net/http"
	"nhooyr.io/websocket"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var clients = entities.NewClients()

func WebsocketConnectHandler(w http.ResponseWriter, r *http.Request) {
	c, err := websocket.Accept(w, r, nil)
	if err != nil {
		log.Println("Could not connect: ", err.Error())
	}
	clients.Add(c)
}

func main() {
	log.Println("Start server")

	http.HandleFunc("/", Index)
	http.HandleFunc("/ws", WebsocketConnectHandler)

	go func() {
		err := http.ListenAndServe(":8000", nil)
		if err != nil {
			log.Fatal("ListenAndServe: ", err)
		}
	}()

	log.Println("Server started")

	p := prompt.New(func(command string) {
		command = strings.TrimSpace(command)
		mainCommand := strings.Split(command, " ")[0]

		switch mainCommand {
		case "sendc":
			argumentValue, err := getArgumentIntValue(command, "id")
			if err != nil {
				log.Println(err.Error())
			} else {
				clients.SendMessageToClient(argumentValue)
			}
		case "send":
			argumentValue, err := getArgumentIntValue(command, "hub")
			if err != nil {
				log.Println(err.Error())
			} else {
				clients.SendMessageToHub(argumentValue)
			}
		case "getClientIds":
			res := clients.GetAllClientIds()
			if res == "" {
				log.Println("There is not any clients")
			} else {
				log.Println(res)
			}
		case "getHubs":
			res := clients.GetHubs()
			if res == "" {
				log.Println("There is not any hubs")
			} else {
				log.Println(res)
			}
		case "exit", "quit":
			log.Println("Bye!")
			os.Exit(0)
		default:
			log.Println("Unknown command")
		}
	}, func(document prompt.Document) []prompt.Suggest {
		return []prompt.Suggest{}
	})

	p.Run()
}

func getArgumentIntValue(command, argName string) (int, error) {
	splitCommand := strings.Split(command, " ")
	if len(splitCommand) < 3 {
		return 0, fmt.Errorf("command is invalid: %s", command)
	}
	argumentName := regexp.MustCompile("^--").ReplaceAllString(splitCommand[1], "")
	argumentValue := splitCommand[2]

	if argumentName != argName {
		return 0, fmt.Errorf("unknown argument: %s", argumentName)
	}

	res, err := strconv.Atoi(argumentValue)
	if err != nil {
		return 0, fmt.Errorf("argument value must be a integer: %s", argumentValue)
	}

	return res, nil
}

func Index(w http.ResponseWriter, req *http.Request) {
	fp, err := os.Open("src/templates/index.html")
	if err != nil {
		log.Println("Could not open file: ", err.Error())
		_, err := w.Write([]byte("500 internal server error"))
		if err != nil {
			log.Println("Could not send 500 error: ", err.Error())
		}
		return
	}

	defer func(fp *os.File) {
		err := fp.Close()
		if err != nil {
			log.Println("Could not close file: ", err.Error())
		}
	}(fp)

	_, err = io.Copy(w, fp)
	if err != nil {
		log.Println("Could not send file contents: ", err.Error())
		_, err2 := w.Write([]byte("500 internal server error"))
		if err2 != nil {
			log.Println("Could not send 500 error: ", err.Error())
		}
		return
	}
}
